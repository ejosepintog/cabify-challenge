import type IProduct from "../interfaces/Product";
import type IDiscount from "../interfaces/Discount";
export default class Checkout {
  availableProducts: IProduct[];

  constructor(availableProducts: IProduct[]) {
    this.availableProducts = availableProducts;
  }

  get discounts(): IDiscount[] {
    const discounts = [];

    for (const product of this.availableProducts) {
      if (this.productHasPercentageDiscount(product))
        discounts.push(this.calculatePercentageDiscount(product));

      if (this.productHasFreeUnitDiscount(product))
        discounts.push(this.calculateFreeUnitDiscount(product));
    }

    return discounts;
  }

  scan(code: string): this {
    const product = this.availableProducts.find(
      (product) => product.code === code
    );

    product!.quantity++;

    return this;
  }

  total(): number {
    const totalAmount = this.availableProducts.reduce(
      (prev: number, curr: IProduct) => {
        return curr.price * curr.quantity + prev;
      },
      0
    );

    const totalDiscount = this.discounts.reduce((prev: number, curr: any) => {
      return prev + curr.amount;
    }, 0);

    return totalAmount - totalDiscount;
  }

  productHasPercentageDiscount(product: IProduct): boolean {
    return (
      !!product.discounts?.percentage &&
      product.discounts.percentage.minimumUnits <= product.quantity
    );
  }

  calculatePercentageDiscount(product: IProduct): IDiscount {
    const productTotal = product.price * product.quantity;
    return {
      text: `x${product.discounts!.percentage!.minimumUnits} ${
        product.name
      } offer`,
      amount: (product.discounts!.percentage!.discount * productTotal) / 100,
    };
  }

  productHasFreeUnitDiscount({ discounts, quantity }: IProduct): boolean {
    return !!discounts?.freeUnit && discounts.freeUnit.every <= quantity;
  }

  calculateFreeUnitDiscount({
    discounts,
    name,
    quantity,
    price,
  }: IProduct): IDiscount {
    const timesToApply = Math.trunc(quantity / discounts!.freeUnit!.every);
    const priceToDiscount =
      (discounts!.freeUnit!.every - discounts!.freeUnit!.quantityToPay) * price;

    return {
      text: `${discounts!.freeUnit!.every}x${
        discounts!.freeUnit!.quantityToPay
      } ${name} offer`,
      amount: priceToDiscount * timesToApply,
    };
  }
}
