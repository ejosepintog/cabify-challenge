export default interface IDiscount {
  text: string;
  amount: number;
}
