export default interface IProduct {
  name: string;
  code: string;
  price: number;
  img: string;
  quantity: number;
  discounts?: IDiscounts;
}

interface IDiscounts {
  percentage?: {
    minimumUnits: number;
    discount: number;
  };
  freeUnit?: {
    every: number;
    quantityToPay: number;
  };
}
